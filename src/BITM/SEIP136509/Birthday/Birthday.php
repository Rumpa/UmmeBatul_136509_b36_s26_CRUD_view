<?php
namespace App\Birthday;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Birthday extends DB{
    public $id="";
    public $name="";
    public $birth_day="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("date",$postVariable)){
            $this->birth_day=$postVariable['date'];
        }
    }

    public function store(){
        $arrData=array($this->name,$this->birth_day);
        $sql="insert into birthday(name,birthday)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
}

?>