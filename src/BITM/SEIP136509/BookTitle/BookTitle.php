<?php
namespace App\BookTitle;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
use PDO;
class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("book_title",$postVariable)){
            $this->book_title=$postVariable['book_title'];
        }
        if(array_key_exists("author_name",$postVariable)){
            $this->author_name=$postVariable['author_name'];
        }
    }

    public function store(){
        $arrData=array($this->book_title,$this->author_name);
        $sql="insert into booktitle(book_title,author_name)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }


    public function index(){

        $STH=$this->DBH->query('SELECT * from booktitle ORDER BY book_title DESC');

            $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrAllData=$STH->fetchAll();
        return $arrAllData;
    }


    public function view(){

        $sql='SELECT * from booktitle where id='.$this->id;

        $STH=$this->DBH->query($sql);
            $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrOneData=$STH->fetch();
        return $arrOneData;
    }
}

?>

