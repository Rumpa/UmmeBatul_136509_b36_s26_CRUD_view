<?php
namespace App\Email;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class Email extends DB{
    public $id="";
    public $name="";
    public $email="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("email",$postVariable)){
            $this->email=$postVariable['email'];
        }
    }

    public function store(){
        $arrData=array($this->name,$this->email);
        $sql="insert into email(name,email)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
}

?>