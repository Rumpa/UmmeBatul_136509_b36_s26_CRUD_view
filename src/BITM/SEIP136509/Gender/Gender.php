<?php
namespace App\Gender;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class Gender extends DB{
    public $id="";
    public $name="";
    public $gender="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("gender",$postVariable)){
            $this->gender=$postVariable['gender'];
        }
    }

    public function store(){
        $arrData=array($this->name,$this->gender);
        $sql="insert into gender(name,gender)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
}

?>

