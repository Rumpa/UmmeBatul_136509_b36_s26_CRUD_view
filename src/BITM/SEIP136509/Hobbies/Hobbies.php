<?php
namespace App\Hobbies;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class Hobbies extends DB{
    public $id="";
    public $name="";
    public $hobbies="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("hobby_name",$postVariable)){
            $this->hobbies=$postVariable['hobby_name'];
        }
    }

    public function store(){
        $checkbox1=$this->hobbies;
        $chk="";
        foreach($checkbox1 as $chk1){
            $chk.=$chk1." ";
        }

        $arrData=array($this->name,$chk);
        $sql="insert into hobbies(name,hobbies)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted Successfully");
        }
        else{
            Message::message("Falied!Data has not been inserted Successfully");
        }

        Utility::redirect('create.php');
    }
}

?>

