<?php
namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Utility\Utility;
use App\Message\Message;
class ProfilePicture extends DB{
    public $id="";
    public $name="";
    public $profile_picture="";

    public function __construct(){
        parent::__construct();
    }

    public function setData($postVariable=NULL){
        if(array_key_exists("id",$postVariable)){
            $this->id=$postVariable['id'];
        }
        if(array_key_exists("name",$postVariable)){
            $this->name=$postVariable['name'];
        }
        if(array_key_exists("profile_picture",$postVariable)){
            $this->profile_picture=$postVariable['profile_picture'];
        }
    }

    public function store(){
        $folder="/xampp/htdocs/LabExam7_Atomic_Project_UmmeBatul_136509_b36/image/";
        $path=$folder.time().$_FILES['profile_picture']['name'];
        $temporary_location=$_FILES['profile_picture']['tmp_name'];
        $img=move_uploaded_file($temporary_location,$path);

        $arrData=array($this->name,$path);
        $sql="insert into profilepicture(name,profile_picture)VALUES (?,?)";
        $STH=$this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result){
            Message::message("Success!Data has been inserted successfully");
        }
        else{
            Message::message("Failed!Data has not been inserted successfully");
        }

        Utility::redirect('create.php');



    }

}

?>


